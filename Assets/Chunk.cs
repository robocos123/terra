﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk {
	
	World world;
	
	MeshFilter meshFilter;
	MeshRenderer meshRenderer;
	MeshCollider collider;
	
	public GameObject chunkObj;
	
	Vector2Int chunkGridPos;
	
	//y-axis is height, so grid is xz plane
	public Vector3Int ChunkGridPos {
		get {
			return new Vector3Int(chunkGridPos.x, 0, chunkGridPos.y);
		}
	}
	
	//stores chunk data as a copy after modification
	public Dictionary<Vector3Int, float> modifiedChunk;
	
	private List<Vector3> vertices = new List<Vector3>();
	private List<Vector3> normals = new List<Vector3>();
	private List<int> triangles = new List<int>();
	
	public Chunk(Vector2Int gridPos) {
		this.chunkGridPos = gridPos;
		
	}
	
}
