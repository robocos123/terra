﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour {
	// Start is called before the first frame update
	
	public bool refresh = false;
	public bool randSeed = false;
	public int numChunks = 4;
	
	[Range(0,1)]
	public float surfaceLevel = 0.5f;
	
	[Min(16)]
	public int chunkWidth = 16;
	[Min(20)]
	public int chunkHeight = 18;
	
	public bool smooth = true;
	public long seed = 0;
	
	//height change frequency
	[Range(0.1f,1.5f)]
	public float noiseScale = 1.0f;
	
	[Range(2,8)]
	public int octaves = 5;
	[Range(1,4)]
	public int octaveWeight = 3;
	
	[Range(1.0f,2.5f)]
	public float lacunarity = 2;
	
	[Range(0.5f,1.5f)]
	public float persistence = 0.8f;
	
	[Min(0.5f)]
	public float noiseWeight = 6;
	
	public float floor = -2;
	
	private void Awake() {
		World.generator = this;
	}
	
	void Start() {
		CreateChunks();
		FillChunks();
	}
	
	void CreateChunks() {
		for(int x = 0; x < numChunks; x++) {
			for(int z = 0; z < numChunks; z++) {
				Vector2Int chunkPos = chunkWidth * new Vector2Int(x, z);
				if(!World.chunks.ContainsKey(chunkPos)) {
					World.chunks.Add(chunkPos, new Chunk(chunkPos));
				}else {
					World.chunks[chunkPos].chunkObject.transform.SetParent(transform);
				}
			}
		}
	}
	
	void FillChunks() {
		if(seed == 0) {
			seed = new System.Random().Next();
		}
		
		World world = World.Instance;
		List<Thread> threads = new List<Thread>();
		
	}

	// Update is called once per frame
	void Update() {
		if(randSeed) {
			seed = 0;
		}
		
		if(refresh || randSeed) {
			randSeed = false;
			refresh = false;
			CreateChunks();
			FillChunks();
		}
	}
}
